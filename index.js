import gsap from 'gsap'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import * as THREE from 'three'
console.log('hot')

const scene = new THREE.Scene();
scene.background = new THREE.Color("rgb(220, 220, 220)")
const camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );

const renderer = new THREE.WebGLRenderer();
renderer.setSize( window.innerWidth, window.innerHeight );
document.body.appendChild( renderer.domElement );

const geometry = new THREE.BoxGeometry(4, 4, 4);
const material = new THREE.MeshLambertMaterial( {wireframe: false, color: 0x00ff00 } );
const cube = new THREE.Mesh( geometry, material );
scene.add( cube );

const circleGeometry = new THREE.CircleGeometry(4, 4);
const circle = new THREE.Mesh( circleGeometry, material );
circle.position.set(0, 0, -2)
scene.add( circle );

// Lights
const light = new THREE.PointLight( new THREE.Color("rgb(255, 255, 255)"), 1, 100 )
light.position.set( 3, 3, 3 )
scene.add( light )

camera.position.z = 10;

const controls = new OrbitControls( camera, renderer.domElement );
controls.update()

function animate() {
    requestAnimationFrame( animate );

    controls.update()
    controls.target = cube.position
    renderer.render( scene, camera );
};

animate();


//gsap animation
//GSAP.TO
// gsap.to(cube.position, { x: '+=5', duration: 3 })

//GSAP.FROMTO
gsap.fromTo(cube.position, {x: -5}, { x: '+=5', duration: 3 })

//GSAP.timeline
const tl = gsap.timeline()
tl.to(cube.position, {x: 5, duration: 2}, 0)
tl.to(cube.position, {x: -5}, 3)
tl.to(cube.position, {y: 5}, '+=.5')
gsap.fromTo(cube.position, {x: -5}, { x: '+=5', duration: 3 })